package com.epam.training.student_maksym_hladii.assessment.test;

import com.epam.training.student_maksym_hladii.assessment.page.*;
import com.epam.training.student_maksym_hladii.assessment.service.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class SpotifyLoginEmptyCredentialsTest extends AbstractTest {

    @BeforeAll
    public static void spotifyLoginEmptyCredentialsTestScript() {
        spotifyLoginFormData = SpotifyLoginFormDataCreator.createSpotifyLoginFormData(
                "testdata.incorrect.username", "testdata.incorrect.password"
        );

        SpotifyHomePage spotifyHomePage = new SpotifyHomePage(driver).openPage();
        SpotifyLoginPage spotifyLoginPage = spotifyHomePage.clickLoginButton()
                .enterUsername(spotifyLoginFormData.getUsername())
                .enterPassword(spotifyLoginFormData.getPassword())
                .clearUsername().clearPassword();

        spotifyLoginFormErrorMessages = SpotifyLoginFormErrorMessagesRetriever.retrieveLoginFormErrorMessages(
                spotifyLoginPage.getUsernameErrorMessage(),
                spotifyLoginPage.getPasswordErrorMessage()
        );
    }

    @Test
    public void spotifyLoginEmptyUsernameTest() {
        Assertions.assertEquals(
                TestDataReader.getTestData("testdata.empty.username.error.message"),
                spotifyLoginFormErrorMessages.getEmptyUsernameErrorMessage()
        );
    }

    @Test
    public void spotifyLoginEmptyPasswordTest() {
        Assertions.assertEquals(
                TestDataReader.getTestData("testdata.empty.password.error.message"),
                spotifyLoginFormErrorMessages.getEmptyPasswordErrorMessage()
        );
    }

}
