package com.epam.training.student_maksym_hladii.assessment.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SpotifyLoginPage extends AbstractPage {

    @FindBy(id = "login-username")
    WebElement usernameField;

    @FindBy(id = "login-password")
    WebElement passwordField;

    @FindBy(id = "login-button")
    WebElement loginButton;

    @FindBy(id = "username-error")
    WebElement usernameErrorMessage;

    @FindBy(id = "password-error")
    WebElement passwordErrorMessage;

    By errorMessageLocator = By.xpath("//span[contains(@class, 'Message')]");

    public SpotifyLoginPage(WebDriver driver) {
        super(driver);
    }

    public SpotifyLoginPage enterUsername(String username) {
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT))
                .until(ExpectedConditions.elementToBeClickable(usernameField));
        usernameField.sendKeys(username);
        return this;
    }

    public SpotifyLoginPage enterPassword(String password) {
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT))
                .until(ExpectedConditions.elementToBeClickable(passwordField));
        passwordField.sendKeys(password);
        return this;
    }

    public SpotifyLoginPage clickLoginButtonFailure() {
        loginButton.click();
        return this;
    }

    public SpotifyHomePage clickLoginButtonSuccess() {
        loginButton.click();
        return new SpotifyHomePage(driver);
    }

    public SpotifyLoginPage clearUsername() {
        usernameField.sendKeys(Keys.CONTROL, Keys.BACK_SPACE);
        return this;
    }

    public SpotifyLoginPage clearPassword() {
        passwordField.sendKeys(Keys.CONTROL, Keys.BACK_SPACE);
        return this;
    }

    public String getUsernameErrorMessage() {
        return usernameErrorMessage.getText();
    }

    public String getPasswordErrorMessage() {
        return passwordErrorMessage.getText();
    }

    public String getErrorMessage() {
        WebElement errorMessage = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT))
                .until(ExpectedConditions.visibilityOfElementLocated(errorMessageLocator));
        return errorMessage.getText();
    }

}
