package com.epam.training.student_maksym_hladii.assessment.service;

import com.epam.training.student_maksym_hladii.assessment.model.SpotifyProfileData;

public class SpotifyProfileDataRetriever {

    public static SpotifyProfileData retrieveSpotifyProfileData(String name) {
        return new SpotifyProfileData(name);
    }

}
