package com.epam.training.student_maksym_hladii.assessment.test;

import com.epam.training.student_maksym_hladii.assessment.page.*;
import com.epam.training.student_maksym_hladii.assessment.service.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class SpotifyLoginCorrectCredentialsTest extends AbstractTest {

    @BeforeAll
    public static void spotifyLoginCorrectCredentialsTestScript() {
        spotifyLoginFormData = SpotifyLoginFormDataCreator.createSpotifyLoginFormData(
                "testdata.correct.username", "testdata.correct.password"
        );

        SpotifyHomePage spotifyHomePage = new SpotifyHomePage(driver).openPage()
                .clickLoginButton()
                .enterUsername(spotifyLoginFormData.getUsername())
                .enterPassword(spotifyLoginFormData.getPassword())
                .clickLoginButtonSuccess();

        spotifyProfileData = SpotifyProfileDataRetriever.retrieveSpotifyProfileData(spotifyHomePage.getName());
    }

    @Test
    public void spotifyLoginCorrectCredentialsTest() {
        Assertions.assertEquals(TestDataReader.getTestData("testdata.name"), spotifyProfileData.getName());
    }

}
