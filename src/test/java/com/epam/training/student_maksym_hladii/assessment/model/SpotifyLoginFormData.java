package com.epam.training.student_maksym_hladii.assessment.model;

public class SpotifyLoginFormData {

    private String username;

    private String password;

    public SpotifyLoginFormData(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
