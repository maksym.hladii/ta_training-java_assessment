package com.epam.training.student_maksym_hladii.assessment.model;

public class SpotifyLoginFormErrorMessages {

    private String emptyUsernameErrorMessage;
    private String emptyPasswordErrorMessage;
    private String errorMessage;

    public SpotifyLoginFormErrorMessages(String emptyUsernameErrorMessage, String emptyPasswordErrorMessage) {
        this.emptyUsernameErrorMessage = emptyUsernameErrorMessage;
        this.emptyPasswordErrorMessage = emptyPasswordErrorMessage;
    }

    public SpotifyLoginFormErrorMessages(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getEmptyUsernameErrorMessage() {
        return emptyUsernameErrorMessage;
    }

    public void setEmptyUsernameErrorMessage(String emptyUsernameErrorMessage) {
        this.emptyUsernameErrorMessage = emptyUsernameErrorMessage;
    }

    public String getEmptyPasswordErrorMessage() {
        return emptyPasswordErrorMessage;
    }

    public void setEmptyPasswordErrorMessage(String emptyPasswordErrorMessage) {
        this.emptyPasswordErrorMessage = emptyPasswordErrorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
