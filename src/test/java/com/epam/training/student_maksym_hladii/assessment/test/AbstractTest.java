package com.epam.training.student_maksym_hladii.assessment.test;

import com.epam.training.student_maksym_hladii.assessment.driver.DriverSingleton;
import com.epam.training.student_maksym_hladii.assessment.model.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;

public abstract class AbstractTest {

    protected static WebDriver driver;

    protected static SpotifyLoginFormData spotifyLoginFormData;
    protected static SpotifyLoginFormErrorMessages spotifyLoginFormErrorMessages;
    protected static SpotifyProfileData spotifyProfileData;

    @BeforeAll
    public static void browserSetup() {
        driver = DriverSingleton.getDriver();
    }

    @AfterAll
    public static void browserStop() {
        DriverSingleton.closeDriver();
    }

}
