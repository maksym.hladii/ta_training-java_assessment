package com.epam.training.student_maksym_hladii.assessment.service;

import com.epam.training.student_maksym_hladii.assessment.model.SpotifyLoginFormErrorMessages;

public class SpotifyLoginFormErrorMessagesRetriever {

    public static SpotifyLoginFormErrorMessages retrieveLoginFormErrorMessages(String usernameErrorMessage,
                                                                               String passwordErrorMessage) {
        return new SpotifyLoginFormErrorMessages(usernameErrorMessage, passwordErrorMessage);
    }

    public static SpotifyLoginFormErrorMessages retrieveLoginFormErrorMessages(String errorMessage) {
        return new SpotifyLoginFormErrorMessages(errorMessage);
    }

}
