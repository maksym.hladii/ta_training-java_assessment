package com.epam.training.student_maksym_hladii.assessment.test;

import com.epam.training.student_maksym_hladii.assessment.page.*;
import com.epam.training.student_maksym_hladii.assessment.service.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class SpotifyLoginIncorrectCredentialsTest extends AbstractTest {

    @BeforeAll
    public static void spotifyLoginIncorrectCredentialsTestScript() {
        spotifyLoginFormData = SpotifyLoginFormDataCreator.createSpotifyLoginFormData(
                "testdata.incorrect.username", "testdata.incorrect.password"
        );

        SpotifyHomePage spotifyHomePage = new SpotifyHomePage(driver).openPage();
        SpotifyLoginPage spotifyLoginPage = spotifyHomePage.clickLoginButton()
                .enterUsername(spotifyLoginFormData.getUsername())
                .enterPassword(spotifyLoginFormData.getPassword())
                .clickLoginButtonFailure();

        spotifyLoginFormErrorMessages = SpotifyLoginFormErrorMessagesRetriever
                .retrieveLoginFormErrorMessages(spotifyLoginPage.getErrorMessage());
    }

    @Test
    public void spotifyLoginIncorrectCredentialsTest() {
        Assertions.assertEquals(
                TestDataReader.getTestData("testdata.incorrect.credentials.error.message"),
                spotifyLoginFormErrorMessages.getErrorMessage()
        );
    }

}
