package com.epam.training.student_maksym_hladii.assessment.service;

import java.util.ResourceBundle;

public class TestDataReader {

    public static String getTestData(String key) {
        String environmentSystemProperty = System.getProperty("environment");
        if (environmentSystemProperty == null) {
            environmentSystemProperty = "dev";
        }
        return ResourceBundle.getBundle(environmentSystemProperty).getString(key);
    }

}
