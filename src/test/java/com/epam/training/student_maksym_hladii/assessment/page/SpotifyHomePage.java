package com.epam.training.student_maksym_hladii.assessment.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SpotifyHomePage extends AbstractPage {

    private static final String SPOTIFY_HOME_PAGE_URL = "https://open.spotify.com/";

    @FindBy(xpath = "//button[@data-testid='login-button']")
    private WebElement loginButton;

    @FindBy(xpath = "//button[@data-testid='user-widget-link']")
    private WebElement name;

    public SpotifyHomePage(WebDriver driver) {
        super(driver);
    }

    public SpotifyHomePage openPage() {
        driver.get(SPOTIFY_HOME_PAGE_URL);
        return this;
    }

    public SpotifyLoginPage clickLoginButton() {
        loginButton.click();
        return new SpotifyLoginPage(driver);
    }

    public String getName() {
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT))
                .until(ExpectedConditions.elementToBeClickable(name));
        return name.getAttribute("aria-label");
    }

}
