package com.epam.training.student_maksym_hladii.assessment.service;

import com.epam.training.student_maksym_hladii.assessment.model.SpotifyLoginFormData;

public class SpotifyLoginFormDataCreator {

    public static SpotifyLoginFormData createSpotifyLoginFormData(String testdataUsername, String testdataPassword) {
        return new SpotifyLoginFormData(
                TestDataReader.getTestData(testdataUsername),
                TestDataReader.getTestData(testdataPassword)
        );
    }

}
