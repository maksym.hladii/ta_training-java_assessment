package com.epam.training.student_maksym_hladii.assessment.model;

public class SpotifyProfileData {

    private String name;

    public SpotifyProfileData(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
